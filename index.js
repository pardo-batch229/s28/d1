db.users.insertOne({
    
        "username": "dahyunKim",
        "password": "once1234"
    
    })
    
db.users.insertOne({
    
        "username": "teejaecalinao",
        "password": "tjc1234"
    
    })

db.users.insertMany([
    {
            "username": "pablo123",
            "password": "123paul"
    },
    {
            "username": "pedro99",
            "password": "iampeter99"
    }
])

//Commands for Creating Documents:
/* 

    db.collection.insertOne({

        "field1": "value",
        "field2": "value"

    })
    db.collection.insertMany([
        
        {
            "field1": "value",
            "field2": "value"
        },
        {
            "field1": "value",
            "field2": "value"
        }
])

*/
//Mini-Activity:
//1. Retrieve/find all car documents.
//2. Retrieve/find all car documnets which type is "sedan".

db.cars.find()

db.cars.find({"type": "sedan"})

// find() - gets all documents that matches the criteria
// findOne() - gets the firstdocument that matches that criteria
db.cars.findOne({})

db.cars.findOne({type: "sedan"})

//updateOne() - allows us update the first item that matches our criteria
db.users.updateOne({username:"pedro99"},{$set:{username:"peter1999"}})
// using {} with no condition in it, it will update the first item that matches the criteria
db.users.updateOne({},{$set:{username:"dahyunieTwice"}})
// if theres no field exist in document it will add a new fields with the corresponding condition that you set
db.users.updateOne({username:"pablo123"},{$set:{isAdmin:true}})
// updateMany() - allows us to update all documents that matches that criteria
db.users.updateMany({},{$set:{isAdmin:true}})

db.cars.updateMany({type:"sedan"},{$set:{price:1000000}})

//deleteOne() - deletes the first document that matches the criteria
//db.users.deleteOne([])
db.cars.deleteOne({brand:"Toyota"})
//deleteMany() - deletes all the items that matches the criteria 
db.users.deleteMany({isAdmin:true})